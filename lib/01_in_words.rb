class Fixnum

  @@word = {
    1 => "one", 2 => "two", 3 => "three",
    4 => "four", 5 => "five", 6 => "six", 7 => "seven",
    8 => "eight", 9 => "nine", 10 => "ten", 11 => "eleven",
    12 => "twelve", 13 => "thirteen", 14 => "fourteen", 15 => "fifteen",
    16 => "sixteen", 17 => "seventeen", 18 => "eighteen", 19 => "nineteen"
  }

  @@tens = {
    2 => "twenty", 3 => "thirty", 4 => "forty", 5 => "fifty",
    6 => "sixty", 7 => "seventy", 8 => "eighty", 9 => "ninety"
  }

  @@powers = {
    2 => "hundred", 3 => "thousand", 6 => "million",
    9 => "billion", 12 => "trillion"
  }

  def in_words
    return "zero" if 0 == self

    result = []

    num = self
    [12, 9, 6, 3, 2].each do |exponent|
      multiple = 10 ** exponent
      if num >= multiple
        result << "#{(num / multiple).in_words} #{@@powers[exponent]}"
        num = num % multiple
      end
    end

    result << @@tens[num / 10]
    result << @@word[num % 10] if num > 20
    result << @@word[num] if num < 20

    result.compact.join(" ")
  end

end
